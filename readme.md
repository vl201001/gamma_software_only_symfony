# Projet gamma_software_only_symfony

Etant encore en train de me former sur Angular, j'ai préféré commencer le test technique en faisant le tout sur Symfony 6.

Je ferai ensuite un projet intégrant Angular.

## Context du projet

Projet consistant à importer un fichier Excel contenant des groupes de musique.

Un fichier exemple se trouve [ici](./private/test.xlsx).

Le projet contient : 
- [x] Un formulaire d'import pour le fichier Excel
- [x] Une interface pour accéder à la liste des groupes importés
- [x] Un formulaire d'ajout / modification de groupe de musique
- [x] Un bouton permettant de supprimer les groupes de musique
- [x] Une fiche de détail pour les groupes de musique
- [x] Une commande permettant d'importer le fichier de test en base de données

## Non inclus dans le projet

Voici les fonctionnalités non demandées pour le projet qu'il serait judicieux de rajouter : 

- Un système d'authentificationn avec une gestion des utilisateurs
- La vérification que le groupe de musique n'aie pas déjà été ajouté avant de l'insérer en base de données
- Une intégration du responsive design pour un support de l'affichage sur mobile
- Une récupération des logs d'import
