<?php

namespace App\Controller;

use App\Entity\MusicGroup;
use App\Factory\MusicGroupFactory;
use App\Form\ImportFileType;
use App\Form\MusicGroupType;
use App\Repository\MusicGroupRepository;
use App\Utils\XlsxReader;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class MusicGroupController extends AbstractController
{
    public function __construct(private MusicGroupRepository $musicGroupRepository)
    {
        
    }

    #[Route('/', methods:['GET', 'POST'])]
    public function list(Request $request, Filesystem $fs, SluggerInterface $slugger, KernelInterface $kernel, LoggerInterface $logger, XlsxReader $xlsxReader): Response
    {
       $form = $this->createForm(ImportFileType::class);
       $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {
           /** @var UploadedFile $file */
            $file = $form->get('file')->getData();

            if ($file) {
                $directoryPath = $kernel->getProjectDir() . '/private/upload/';

                if (!$fs->exists($directoryPath)) {
                    $fs->mkdir($directoryPath);
                }

                $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFileName = $slugger->slug($originalName);
                $newFileName = $safeFileName . '-' . uniqid() . '.' . $file->guessExtension();

                try {
                    $file->move(
                        $directoryPath,
                        $newFileName
                    );

                    // Read the Xslx file
                    $arrayData = $xlsxReader->readFile($directoryPath . $newFileName);

                    // Prepare insertion
                    foreach ($arrayData as $item) {
                        $this->musicGroupRepository->save(MusicGroupFactory::createFromArray($item));
                    }

                    // Inserts in database
                    $this->musicGroupRepository->flush();

                } catch (FileException $e) {
                    $logger->critical($e->getMessage());
                }

            }
       }

        $list = $this->musicGroupRepository->findAll();

       return $this->render('music_groups/list.html.twig', [
           'list' => $list,
           'form' => $form->createView()
       ]);
    }

    #[Route('/view/{id}', methods:['GET'])]
    public function view(MusicGroup $musicGroup): Response
    {
        return $this->render('music_groups/view.html.twig', [
            'music_group' => $musicGroup
        ]);
    }

    #[Route('/edit/{id}', methods: ['GET', 'POST'])]
    public function edit(Request $request, MusicGroup $musicGroup): Response
    {
        $form = $this->createForm(MusicGroupType::class, $musicGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->musicGroupRepository->flush();

            return $this->redirectToRoute('app_musicgroup_list');
        }

        return $this->render('music_groups/edit.html.twig', [
            'music_group' => $musicGroup,
            'form' => $form->createView()
        ]);
    }

    #[Route('/add', methods: ['GET', 'POST'])]
    public function add(Request $request): Response
    {
        $musicGroup = new MusicGroup();

        $form = $this->createForm(MusicGroupType::class, $musicGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->musicGroupRepository->save($musicGroup, true);

            return $this->redirectToRoute('app_musicgroup_list');
        }

        return $this->render('music_groups/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/delete/{id}')]
    public function delete(Request $request, MusicGroup $musicGroup): Response
    {
        if ($this->isCsrfTokenValid('delete' . $musicGroup->getId(), $request->request->get('_token'))) {
            $this->musicGroupRepository->remove($musicGroup, true);
        }

        return $this->redirectToRoute('app_musicgroup_list');
    }

    #[Route('/download/sample', methods: ['GET'])]
    public function downloadSampleFileToImport(KernelInterface $kernel): Response
    {
        $filePath = $kernel->getProjectDir() . '/private/test.xlsx';
        $fileContent = file_get_contents($filePath);

        $response = new Response();

        // Set headers
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename=exemple.xlsx');

        $response->setContent($fileContent);

        return $response;
    }
}
