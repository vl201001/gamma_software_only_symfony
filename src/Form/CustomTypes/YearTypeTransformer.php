<?php

namespace App\Form\CustomTypes;

use Symfony\Component\Form\DataTransformerInterface;

class YearTypeTransformer implements DataTransformerInterface
{

    public function transform(mixed $value)
    {
        if (is_null($value)) {
            return -1;
        } else if (intval($value->format('Y')) < 1900 || intval($value->format('Y')) > intval((new \DateTimeImmutable())->format('Y'))) {
            return -1;
        }

        return intval($value->format('Y'));
    }

    public function reverseTransform(mixed $value)
    {
        if (is_null($value) || $value === -1) {
            return null;
        }

        return \DateTimeImmutable::createFromFormat('d-m-Y', '01-01-' . $value);
    }
}