<?php

namespace App\Form;

use App\Entity\MusicGroup;
use App\Form\CustomTypes\YearType;
use App\Form\CustomTypes\YearTypeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MusicGroupType extends AbstractType
{
    public function __construct(private YearTypeTransformer $yearTypeTransformer) { }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'Nom du groupe',
            ])
            ->add('origin', TextType::class, [
                'required' => true,
                'label' => 'Origine',
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'label' => 'Ville',
            ])
            ->add('startedAt', ChoiceType::class, [
                'required' => true,
                'label' => 'Année début',
                'choices' => $this->getYearChoices(),
            ])
            ->add('endedAt', ChoiceType::class, [
                'required' => false,
                'label' => 'Année séparation',
                'choices' => $this->getYearChoices(),
            ])
            ->add('founders', TextType::class, [
                'required' => false,
                'label' => 'Fondateurs',
            ])
            ->add('nbMembers', NumberType::class, [
                'required' => false,
                'label' => 'Membres',
            ])
            ->add('musicType', TextType::class, [
                'required' => false,
                'label' => 'Courant musical',
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => 'Présentation',
            ])
        ;

        $builder->get('startedAt')
            ->addModelTransformer($this->yearTypeTransformer);

        $builder->get('endedAt')
            ->addModelTransformer($this->yearTypeTransformer);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MusicGroup::class,
        ]);
    }

    private function getYearChoices(): array {
        $now = intval((new \DateTimeImmutable())->format('Y'));
        $choices = [];

        $choices[''] = -1;

        for ($i = 1900; $i <= $now; $i++) {
            $choices[$i] = $i;
        }

        return $choices;
    }
}
