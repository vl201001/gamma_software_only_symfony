<?php

namespace App\Validator;

use App\Entity\MusicGroup;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class MusicGroupValidator
{
    /**
     * @param ?MusicGroup $value
     * @param ExecutionContextInterface $context
     * @param mixed $payload
     * @return void
     */
    public static function validate(mixed $value, ExecutionContextInterface $context, mixed $payload): void
    {
        if (!is_null($value)) {

            // Start date must be before end date
            if (!is_null($value->getStartedAt()) && !is_null($value->getEndedAt())) {
                if ($value->getStartedAt() >= $value->getEndedAt()) {
                    $context->buildViolation('La date de séparation doit être après la date de début')
                        ->atPath('endedAt')
                        ->addViolation();
                }
            }

        }
    }
}